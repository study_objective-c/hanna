//
//  Fraction.m
//  fraction
//
//  Created by kico on 18/06/2019.
//  Copyright © 2019 kico. All rights reserved.
//
/*
 @Interface: 클래스를 사용하기 위한 외부 연결 선언 부분
 @implementation: 메소드 함수 정의 부분
 */

#import "Fraction.h"
@implementation Fraction

-(void)setNum:(int)n
{
    num = n;
}
-(void)senDen:(int)d
{
    den = d;
}
-(int)num
{
    return num;
}
-(int)den
{
    return den;
}
-(void)setTo:(int)n over:(int)d
{
    num = n;
    den = d;
    [self reduce]; //자신의 인스턴스 메서드를 호출
}
-(void)reduce
{
    int u = num;
    int v = den;
    int temp;
    while(v != 0)
    {
        temp = u % v;
        u = v;
        v = temp;
    }
    
    num /= u;
    den /= u;
}
-(void)print
{
    printf("%i\/%i\n", num, den);
}
-(float)convertsToFloat
{
    if(den != 0)
        return (float)num/ (float)den;
    else
        return 1.0;
}
-(void)add:(Fraction *)f
{
    //동일 클래스의 다른 인스턴스 접근 연산자
    //num = num * f->den + den * f->num 인수로 전달 받은 인스턴스 f가 클래스인 경우 내부 멤버변수에 접근할 수 있는 접근자(->)사용
    //den = den * f->den;
    num = num * [f den] + den * [f num];
    den = den * [f den]; //getter를 통한 멤버변수 접근, 결과는 동일
    [self reduce]; //자신의 인스턴스 메소드를 호출
    return;
}

@end
