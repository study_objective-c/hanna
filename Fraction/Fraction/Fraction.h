//
//  Fraction.h
//  fraction
//
//  Created by kico on 18/06/2019.
//  Copyright © 2019 kico. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction : NSObject
{

    
//멤버변수 선언블록
    int num; //분자
    int den; //분모
}

//자동 생성 접근자,
//@syntherize는 지시어를 사용하여 @property로 성정된 인스턴스 변수의 getter/setter를 자동으로 생성해준다.
//프로퍼티로 선언했으면 getter, setter 선언하지 않아도 됨
//@property(readwrite) int num;
//@property(readwrite) int den;

//메소드 함수 선언

//하나의 매개변수를 가지는 메소드
-(void)setNum:(int)n;
-(void)senDen:(int)d;

//매개변수가 없는 메소드
-(int)num; //set
-(int)den; //set

//여러 개의 매개변수를 가지는 메소드
//호출 방법 [a setTo:1 over:4];
//분수의 분자 분모를 한번에 설정해줌
-(void)setTo:(int)n over:(int)d; //분자 분모를 넣어줌
-(void)reduce;
-(void)print;
-(float)convertsToFloat;
-(void)add:(Fraction *)f; //인수로 전달된 인스턴스 포인터 

@end
