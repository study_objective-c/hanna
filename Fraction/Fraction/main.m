//
//  main.m
//  fraction
//
//  Created by kico on 18/06/2019.
//  Copyright © 2019 kico. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <malloc/malloc.h>
#import "Fraction.h"

int main(int argc, const char * argv[]) {
    Fraction *a, *b;
    
    
 /*
     a = [Fraction alloc];
     a = [a init];
 */
    
    a = [[Fraction alloc] init]; //Fraction 클래스에서 인스턴스를 생성하는 alloc 과 init 메소드
    b = [[Fraction alloc] init];
    [a setTo:1 over:4]; // a = 1/4
    [b setTo:1 over:2]; // b = 1/2
    [a print];
    printf("+\n");
    [b print];
    printf("---\n=");
    [a add:b]; //인스턴스 a에 add: 메소드가 호출된다. 이때, 인수 인스턴스 b의 멤버변수 값은 변하지 않고, a 인스턴스 메소드만 변경되는 것에 주의!
    [a print];
    //[a release];
    //[b release];
    
    return 0;
}
