//
//  main.m
//  empty_static
//
//  Created by kico on 18/06/2019.
//  Copyright © 2019 kico. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Empty.h"

int main(int argc, const char * argv[]) {
    
    Empty *a = [[Empty alloc] init];
    Empty *b = [[Empty alloc] init];
    
    [Empty settingStatic:5];
    [Empty settingStatic:4];
    
    NSLog(@"인스턴스 메소드로 클래스 변수 접근: %d, %d", [a gettingStatic], [b gettingStatic]);
    NSLog(@"외부 링크 함수로 클래스 변수 접근: %d", gettingStatic());
    
    settingStatic(6);
    
    NSLog(@"인스턴스 메소드로 클래스 변수 접근: %d, %d", [a gettingStatic], [b gettingStatic]);
    NSLog(@"외부 링크 함수로 클래스 변수 접근: %d", gettingStatic());
    [a release]; //not available in automatic references counting mode, 현재 자동 메모리 풀어줌 모드여서 안해도됨
    [b release];
     
    return 0;
}
