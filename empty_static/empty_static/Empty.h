//
//  empty.h
//  empty_static
//
//  Created by kico on 18/06/2019.
//  Copyright © 2019 kico. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Empty : NSObject
{
    //static int i; //인터페이스 구역에는 static 변수 선언 불가!!
}

-(int)gettingStatic; //인스턴스 메소드, 인스턴스에서만 작동
+(void)settingStatic:(int)j; //클래스 메소드

@end

extern int gettingStatic(void); //거의 이렇게 사용안함
extern void settingStatic(int);
