//
//  empty.m
//  empty_static
//
//  Created by kico on 18/06/2019.
//  Copyright © 2019 kico. All rights reserved.
//

#import "Empty.h"

//static ins s; //외부에 선언되면 해당 모듈에서 공유, @implementation 내부와 중복 선언시 한쪽은 무시함

@implementation Empty

static int s;
-(int)gettingStatic
{
    return s;
}
+(void)settingStatic:(int)j
{
    s = j;
}
@end


// empty.m 에 정의된 일반함수도 클래스 변수에 접근/설정이 가능
// 클래스 변수는 단지 클래스 메소드 정의 모듈에 선언된 전역변수에 불과!
int gettingStatic(void)
{
    return s;
}

void settingStatic(int j)
{
    s = j;
}
