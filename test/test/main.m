//
//  main.m
//  test
//
//  Created by kico on 18/06/2019.
//  Copyright © 2019 kico. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parent : NSObject
@end

@implementation Parent
@end

@interface Child : Parent
@end

@implementation Child
@end


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        int iValue = 100;
        float fValue = 331.79;
        double fDouble = 1.44e+11;
        double gDouble = 1.44e+01;
        char chValue = 'w';
        
        printf("%d %f %g %g %c\n", iValue, fValue, fDouble, gDouble, chValue);
        
        id p, c;
        
        p = [[Parent alloc] init];
        c = [[Child alloc] init];
        NSLog(@"%p %@", p, p);
        NSLog(@"%p %@", c, c);
        
        printf("              char : %d\n", (int)sizeof(char));
        printf("             short : %d\n", (int)sizeof(short));
        printf("               int : %d\n", (int)sizeof(int));
        printf("              long : %d\n", (int)sizeof(long));
        printf("         long long : %d\n", (int)sizeof(long long));
        printf("unsigned      char : %d\n", (int)sizeof(unsigned char));
        printf("unsigned     short : %d\n", (int)sizeof(unsigned short));
        printf("unsigned       int : %d\n", (int)sizeof(unsigned int));
        printf("unsigned      long : %d\n", (int)sizeof(unsigned long));
        printf("unsigned long long : %d\n", (int)sizeof(unsigned long long));
        printf("             float : %d\n", (int)sizeof(float));
        printf("            double : %d\n", (int)sizeof(double));

        
        printf("CHAR_MIN: %i\n", CHAR_MIN);
        printf("CHAR_MAX   : %i\n",   CHAR_MAX);
        printf("SHRT_MIN   : %i\n",   SHRT_MIN);
        printf("SHRT_MAX   : %i\n",   SHRT_MAX);
        printf("INT_MIN    : %i\n",   INT_MIN);
        printf("INT_MAX    : %i\n",   INT_MAX);
        printf("LONG_MIN   : %li\n",  LONG_MIN);
        printf("LONG_MAX   : %li\n",  LONG_MAX);
        printf("LLONG_MIN  : %lli\n", LLONG_MIN);
        printf("LLONG_MAX  : %lli\n", LLONG_MAX);
        printf("UCHAR_MAX  : %u\n",   UCHAR_MAX);
        printf("USHRT_MAX  : %u\n",   USHRT_MAX);
        printf("UINT_MAX   : %u\n",   UINT_MAX);
        printf("ULONG_MAX  : %lu\n",  ULONG_MAX);
        printf("ULLONG_MAX : %llu\n", ULLONG_MAX);
    }
    return 0;
}
