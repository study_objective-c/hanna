//
//  main.m
//  initialize
//
//  Created by kico on 18/06/2019.
//  Copyright © 2019 kico. All rights reserved.
//
/*
 @Interface: 클래스를 사용하기 위한 외부 연결 선언 부분
 @implementation: 메소드 함수 정의 부분
 */

#import <Foundation/Foundation.h>
@interface A : NSObject
+(void)initialize;
@end

@implementation A
+(void)initialize{ printf("Class A initialized\n"); }
//init 메소드와 달리 [super initialize] 호출 하면 안됨 > 시스템이 자동으로 만듦
@end


@interface B : A
//+(void)initialize;
@end


@implementation B
//+(void)initialize { printf("Class B initialized\n"); }
// Class B를 위한 initialize가 미정의된 이유로 슈퍼클래스 A의 initialize가 대신호출
@end
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        id obj = [[B alloc] init];
        NSLog(@"%@", [obj self]);
        NSLog(@"%@", [B self]);
       // [obj release];
    }
    return 0;
}
